# EDSCC Plug-in

A plugin for [Elite Dangerous Market Connector](https://github.com/Marginal/EDMarketConnector/releases/latest) that transmits commander and selected journal data to [EDBGS](https://edbgs.ru) to aggregate data for data visualization of your squadron.

## How to install:  
1. Download this plugin.

2. Open Elite Dangerous Market Connector and go to File -> Settings. Then browse to the plugins tab.

3. Click "Open" to open the plugins directory.

4. Open the Zip file we have downloaded and drag the folder from within into the plugins directory.  

5. Restart Elite Dangerous Market Connector and go to File -> Settings, select EDBGS tab.  Check the checkbox to activate.

6. Enter your 'Name CMDR' and 'EDBGS API Server' (https://edbgs.ru/adminable/plugin). 

7. Go back to ED:Market Connector and enter it in the settings.

8. Start up Elite Dangerous and start flying!
