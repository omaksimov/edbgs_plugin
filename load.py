# from urllib.parse import quote
import sys
# import ttk
import requests
import json
import time
from queue import Queue
from threading import Thread

import tkinter as tk
from ttkHyperlinkLabel import HyperlinkLabel
import myNotebook as nb

from config import appname, applongname, appversion, config
import companion
import plug
import sys
import codecs

sys.stdout.reconfigure(encoding='utf-8')
sys.stderr.reconfigure(encoding='utf-8')

this = sys.modules[__name__]
this.session = requests.Session()
this.queue = Queue()
this.msg = " "
this.cmdr = None
this.defaultApiHost = 'https://edbgs.ru/adminable/plugin'

_TIMEOUT = 20
_EDBGS_LOG = True
_EDBGS_LOG_LEVEL = 2

def edbgs_log(msg, level=1):
    if (_EDBGS_LOG and _EDBGS_LOG_LEVEL >= level):
        print('EDBGS: %s - %s' % (time.asctime(), msg))

def plugin_start3(plugin_dir):
    edbgs_log('Start Plugin', 0)
    this.thread = Thread(target=worker, name='EDBGS Worker')
    this.thread.daemon = True
    this.thread.start()
    return 'EDBGS'

def plugin_stop():
    # Signal thread to close and wait for it
    this.queue.put(None)
    this.thread.join()
    this.thread = None

def cmdr_data(data, is_beta):
    this.cmdr = data['commander']['name']

    if config.getint('edbgs_out') and not is_beta and credentials(this.cmdr):
        form_data = {
            'commander': data['commander'],
            'lastSystem': data['lastSystem'],
            'ship': data['ship']
        }
        this.queue.put((this.cmdr, 'commander', form_data))
        edbgs_log('Added cmdr_data to queue')

def journal_entry(cmdr, is_beta, system, station, entry, state):
    entry['timestamp'] = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())
    if config.getint('edbgs_out') and not is_beta and credentials(cmdr):
        edbgs_log('New Event: %s' % entry['event'], 2)
        form_data = []

#        if entry['event'] in ['Statistics', 'LoadGame', 'Commander', 'Rank', 'Progress', 'Statistics', 'Docked',
#                              'Undocked', 'Bounty', 'CapShipBond', 'FactionKillBond', 'RedeemVoucher',
#                              'MultiSellExplorationData', 'SellExplorationData', 'SAAScanComplete', 'MarketBuy',
#                              'MarketSell', 'MiningRefined', 'CommunityGoalReward', 'MissionCompleted', 'CommitCrime',
#                              'SquadronStartup', 'AppliedToSquadron', 'LeftSquadron', 'RedeemVoucher']:
        if entry['event'] not in [
                            'NpcCrewPaidWage','EngineerProgress',
                            'Music','ReceiveText','SellDrones','FSSSignalDiscovered','FSSDiscoveryScan','StartUp',
                            'FSDTarget','RefuelAll','ReservoirReplenished','DockingRequested','DockingGranted',
                            'SupercruiseExit','EjectCargo','Repair','RepairAll','WingLeave',
                            'Friends','ApproachBody','BuyDrones','ModuleRetrieve',
                            'StoredModules','ModuleSell','ModuleBuy','Outfitting','ModuleInfo','Cargo',
                            'ShipyardSwap','Shipyard','ShipyardTransfer','StoredShips','Loadout',
                            'ProspectedAsteroid','LaunchDrone','HeatWarning','SendText','FSSAllBodiesFound',
                            'ShipTargeted','SupercruiseEntry','UnderAttack','CrewMemberRoleChange',
                            'MaterialCollected','CrewMemberJoins','CrewMemberQuits','CrewLaunchFighter',
                            'PayFines','FuelScoop','BuyAmmo','RestockVehicle','StartJump','ApproachBody',
                            'ApproachSettlement','BuyDrones','DockFighter','EndCrewSession',
                            'CrewAssign','CrewMemberJoins','CrewMemberQuits','CrewMemberRoleChange',
                            'DockingCancelled','DockingDenied','DockingGranted','DockingRequested',
                            'Powerplay','PowerplayCollect','PowerplayDeliver','PowerplayFastTrack','PowerplaySalary',
                            'Scan','Scanned','VehicleSwitch','StoredShips','USSDrop','Market',
                            'WingAdd','WingInvite','WingJoin','WingLeave','SAASignalsFound','SAAScanComplete',
                            'Reputation','Rank','ModuleStore','NpcCrewRank','Resurrect','LeaveBody',
                            'Materials','LaunchFighter','Synthesis','HullDamage','MiningRefined','CollectCargo',
                            'BuyTradeData','BuyExplorationData','CargoDepot','DatalinkScan','DatalinkVoucher',
                            'FighterDestroyed','FighterRebuilt','HeatDamage','Interdicted','MaterialTrade',
                            'EscapeInterdiction','MaterialDiscovered','Liftoff','DockSRV','LaunchSRV',
                            'Touchdown','CodexEntry','DataScanned','EngineerCraft','Interdiction','ModuleSellRemote',
                            'ModuleSwap','SetUserShipName','ShipyardBuy','ShipyardNew','ShipyardSell','PayFines',
                            'RebootRepair'
                            ]:

            form_data.append(entry)

        if form_data:
            this.queue.put((cmdr, 'journal', form_data))
            edbgs_log("Added '%s' event to queue" % entry['event'], 2)

def plugin_prefs(parent, cmdr, is_beta):

    PADX = 10
    BUTTONX = 12  # indent Checkbuttons and Radiobuttons
    PADY = 2  # close spacing

    frame = nb.Frame(parent)
    frame.columnconfigure(1, weight=1)

    HyperlinkLabel(frame, text='EDBGS', background=nb.Label().cget('background'), url='https://edbgs.ru/',
                   underline=True).grid(columnspan=2, padx=PADX, sticky=tk.W)  # Don't translate
    this.log = tk.IntVar(value=config.getint('edbgs_out') and 1)
    this.log_button = nb.Checkbutton(frame, text=_('Send flight log and Cmdr status to EDBGS'), variable=this.log,
                                     command=prefsvarchanged)
    this.log_button.grid(columnspan=2, padx=BUTTONX, pady=(5, 0), sticky=tk.W)

    nb.Label(frame).grid(sticky=tk.W)  # big spacer
    this.label = HyperlinkLabel(frame, text=_('EDBGS credentials'), background=nb.Label().cget('background'),
                                url='https://edbgs.ru/',
                                underline=True)  # Section heading in settings
    this.label.grid(columnspan=2, padx=PADX, sticky=tk.W)

    this.namecmdr_label = nb.Label(frame, text=_('Name CMDR'))  # EDBGS setting
    this.namecmdr_label.grid(row=12, padx=PADX, sticky=tk.W)
    this.namecmdr = nb.Entry(frame)
    this.namecmdr.grid(row=12, column=1, padx=PADX, pady=PADY, sticky=tk.EW)

    this.edbgshost_label = nb.Label(frame, text=_('EDBGS API Server'))  # EDBGS setting
    this.edbgshost_label.grid(row=14, padx=PADX, sticky=tk.W)
    this.edbgshost = nb.Entry(frame)
    this.edbgshost.grid(row=14, column=1, padx=PADX, pady=PADY, sticky=tk.EW)

    prefs_cmdr_changed(cmdr, is_beta)

    return frame

def prefs_cmdr_changed(cmdr, is_beta):
    this.log_button['state'] = cmdr and not is_beta and tk.NORMAL or tk.DISABLED
    this.namecmdr['state'] = tk.NORMAL
    this.edbgshost['state'] = tk.NORMAL
    this.namecmdr.delete(0, tk.END)
    this.edbgshost.delete(0, tk.END)
    if cmdr:
        cred = credentials(cmdr)
        edbgshost = apihost(cmdr)
        if cred:
            this.namecmdr.insert(0, cred)
            this.edbgshost.insert(0, edbgshost)
    this.label['state'] = this.namecmdr_label['state'] = this.namecmdr['state'] = this.edbgshost_label['state'] = \
        this.edbgshost['state'] = cmdr and not is_beta and this.log.get() and tk.NORMAL or tk.DISABLED

def prefsvarchanged():
    this.label['state'] = this.namecmdr_label['state'] = this.namecmdr['state'] = this.edbgshost_label['state'] = \
        this.edbgshost['state'] = this.log.get() and this.log_button['state'] or tk.DISABLED

def prefs_changed(cmdr, is_beta):
    config.set('edbgs_out', this.log.get())

    if cmdr and not is_beta:
        this.cmdr = cmdr
        this.FID = None
        cmdrs = config.get('edbgs_cmdrs') or []
        namecmdrs = config.get('edbgs_namecmdrs') or []
        edbgshosts = config.get('edbgs_edbgshosts') or []
        edbgs_log('Current Configs: %s %s %s %s' % (cmdrs, cmdrs, namecmdrs, edbgshosts), 2)
        if cmdr in cmdrs:
            idx = cmdrs.index(cmdr)
            namecmdrs.extend([''] * (1 + idx - len(namecmdrs)))
            namecmdrs[idx] = this.namecmdr.get().strip()
            edbgshosts.extend([''] * (1 + idx - len(edbgshosts)))
            edbgshosts[idx] = this.edbgshost.get().strip()
        else:
            config.set('edbgs_cmdrs', cmdrs + [cmdr])
            namecmdrs.append(this.namecmdr.get().strip())
            edbgshosts.append(this.edbgshost.get().strip())
        config.set('edbgs_namecmdrs', namecmdrs)
        config.set('edbgs_edbgshosts', edbgshosts)
        edbgs_log('Configs saved: %s %s' % (namecmdrs, edbgshosts), 2)

def credentials(cmdr):
    # Credentials for cmdr
    if not cmdr:
        return None

    cmdrs = config.get('edbgs_cmdrs') or []
    if cmdr in cmdrs and config.get('edbgs_namecmdrs'):
        return config.get('edbgs_namecmdrs')[cmdrs.index(cmdr)]
    else:
        return None

def apihost(cmdr):
    # Credentials for cmdr
    if not cmdr:
        return None

    cmdrs = config.get('edbgs_cmdrs') or []

    if cmdr in cmdrs and config.get('edbgs_edbgshosts'):
        host = config.get('edbgs_edbgshosts')[cmdrs.index(cmdr)].strip() or this.defaultApiHost
        return host
    else:
        return this.defaultApiHost

# Worker thread
def worker():
    while True:
        item = this.queue.get()
        if not item:
            return  # Closing
        else:
            (cmdr, data_type, form_data) = item
            (nameCmdr) = credentials(cmdr)
            (edbgshost) = apihost(cmdr)

        edbgs_log('Sending event to EDBGS', 1)
        retrying = 0
        while retrying < 3:
            try:
                data = {
                    'fromSoftware': applongname,
                    'fromSoftwareVersion': appversion,
                    'data': json.dumps(form_data, ensure_ascii=False).encode('utf-8'),
                    'data_type': data_type
                }
                header = {
                    'x-name-cmdr': nameCmdr
                }
                r = this.session.post(edbgshost, data=data, timeout=_TIMEOUT, headers=header)
                this.msg = ''
                #reply = r.json()

                if r.status_code == 200:
                    edbgs_log('Event received. Post Successful.', 1)
                    this.msg = 'EDBGS Post Succeeded.'
                else:
                    edbgs_log('EDBGS API Post Fail: [' + str(r.status_code) + '] ')
#                    edbgs_log('API Post Fail: [' + str(r.status_code) + '] ' + reply['message'], 1)
#                    edbgs_log('JSON dump: %s' % json.dumps(data, ident=2, separators=(',', ': ')), 2)
                    this.msg = 'EDBGS Post Failed.'

                if this.msg:
                    # Log fatal errors
                    plug.show_error(_(this.msg))
                break
            except Exception as e:
#            except:
#                if __debug__: print_exc()
                edbgs_log(e)
                retrying += 1
                edbgs_log('in exception, retrying %s' % retrying)
        else:
            plug.show_error(_("Error: Can't connect to EDBGS"))
